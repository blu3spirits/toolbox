#!/bin/bash
# Removes line entry from ~/.ssh/known_hosts
# for host identifier changes
# Shamelessly stolen from Kiro
sed -ie "$@"d ~/.ssh/known_hosts
